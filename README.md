Nuestro granito de arena para reducir las muertes prevenibles.

Queremos ayudar a las personas a entender los resultados de sus estudios de laboratorio,
para que sepan qué hacer.

->  Saber no es suficiente para tomar acción,
    pero podemos simplificar el proceso para que lo hagan.

**Queremos refererir a los pacientes con los médicos que necesitan.**

Queremos conseguir financiamiento para atención de salud
para personas que no tienen los medios.

Queremos ayudar a los médicos a brindar una atención de calidad a los pacientes.

Queremos ayudar a sistemas de salud a mejorar la calidad de su atención
y optimizar la efectividad de sus recursos.

Queremos recabar indicadores de salud para investigación.

Queremos interconectar el historial de salud de los diferentes sistemas médicos.

Queremos empoderar a las personas para ser dueños de su información médica.

- - -

Para personas de bajos recursos
que no pueden pagar una consulta médica para interpretar sus análisis de laboratorio
el [nombre de producto]
es un sitio web y aplicación móvil
que les ayuda a consultar al especialista apropiado para mejorar su salud
y a diferencia de [Lab Tests Online](https://labtestsonline.es/ )
nuestro producto hace la interpretación completa
y te refiere con un profesional adecuado.


¿Quienes podrían interesarse?
-----------------------------

Cuando una persona recibe exámenes de laboratorio con
algún indicador fuera de rango.

Doctores que quieren tener más pacientes.

Aseguradoras de gastos médicos menores como [VRIM](https://www.vrim.com.mx )

Personas que quieren ayudar a otros y tienen un dinerito de sobra.


¿Cuándo se hacen análisis las personas?
---------------------------------------

-   Al entrar a un trabajo.

-   Por parte del trabajo.

-   Cuando hay un problema de salud latente.

-   Como proceso de rutina de algún tema.

-   En tu chequeo médico periódico.
