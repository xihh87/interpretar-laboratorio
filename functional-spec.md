Jorge Buendía, preocupado por su salud encuentra el servicio
-----------------------------------------------------

Jorge Buendía sus resultados de laboratorio
y encuentra que sus niveles de colesterol están por las nubes,
se preocupa y busca en internet qué significan.

Encuentra el servicio de interpretación de resultados de laboratorio
y sube sus resultados de laboratorio.

El sistema le pide algunos datos importantes para la evaluación del perfil
y avisa del tiempo en que se tendrá la recomendación médica.

Además, solicita que se acepten:

-   La política de privacidad
-   Los términos y condiciones del servicio

Opcionalmente, les pide participar en un proyecto de investigación
que correlaciona el consumo de helado con el tejido adiposo marrón,
y acepta que sus datos se usen en otros proyectos de investigación en el futuro.

El sistema solicita un correo para enviar
una recomendación médica a partir de sus análisis de laboratorio.


Daniela Garza revisa los resultados del análisis clínico
--------------------------------------------------------

Mientras está tomando su café,
Daniela recibe un correo que le informa que hay 6 pacientes
al borde del pánico que quieren saber qué significan sus análisis.

Da click en el correo y entra a la plataforma.

Da click en el registro de Jorge Buendía que se encuentra anonimizado).

Entra a una página que muestra el documento de los análisis de laboratorio
y un formulario le ayuda a construir su recomendación médica de manera sencilla.
